<h1>HTS</h1>

<h2>인스톨</h2>
<pre><code>
pip install -r requirements.txt
</code></pre>


<h2>빌드</h2>
<pre><code>
가상환경 실행 : .\venv_kk\Scripts\activate 
build.bat 실행
</code></pre>


<h2>실행</h2>
<pre><code>
hts_program 폴더 내부
main.exe 더블클릭
</code></pre>


<h2>삭제</h2>
<pre><code>
clear.bat 실행
</code></pre>

