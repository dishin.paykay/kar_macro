from commons.classes.chrome_driver import *
from commons.classes.log import *
from commons.function.common_function import *

import pandas as pd
from urllib import parse
import math


MAX_ROW = 1000000

class MainService():

    def __init__(self):
        self.log = Log()
        self.log.create_log()

        self.chrome_driver = ChromeDriver()

        self.url = "http://www.kar.or.kr/pinfo/brokersearch.asp"    # 고정 주소

        self.name_list = []             # 상호
        self.regnum_list = []           # 개설등록번호
        self.ceo_name_list = []         # 대표자
        self.addr_list = []             # 사무실 소재지
        self.tel_list = []              # 전화번호
        self.deduction_yn_list = []     # 공제 가입여부
        
        self.is_last_data = False
    
    def main(self, input):
        """ 
        def description : 메인

        Parameters
        ----------
        do : 시도
        si_gun : 시군
        dong_myun_li : 동면리
        gubun : 검색구분
        keyword : 검색 키워드

        Returns
        -------
        result : {}
        """

        # init 
        query_str = ""

        # 입력 값 예외처리
        if "do" in input:
            if "si_gun" not in input:
                return {"result" : "시/군을 입력해주세요"}
        
        if "si_gun" in input:
            if "do" not in input:
                return {"result" : "시/도를 입력해주세요"}
        
        if "dong_myun_li" in input:
            if "do" not in input:
                return {"result" : "시/도를 입력해주세요"}
            
            if "si_gun" not in input:
                return {"result" : "시/군을 입력해주세요"}

        # 데이터 셋팅
        if "do" in input:
            if input["do"]: 
                addr1 = parse.quote(input["do"], encoding="euc-kr")
                query_str += f"&addr1={addr1}"
        
        if "si_gun" in input:
            if input["si_gun"]: 
                addr2 = parse.quote(input["si_gun"], encoding="euc-kr")
                query_str += f"&addr2={addr2}"

        if "dong_myun_li" in input:
            if input["dong_myun_li"]: 
                addr3 = parse.quote(input["dong_myun_li"], encoding="euc-kr")
                query_str += f"&addr3={addr3}"

        if "gubun" in input:
            if input["gubun"]: 
                where = input["gubun"]
                query_str += f"&where={where}"

        if "keyword" in input:
            if input["keyword"]: 
                query = parse.quote(input["keyword"], encoding="euc-kr")
                query_str += f"&query={query}"

        # driver 이닛
        self.chrome_driver.set_avoided_drive()
        time.sleep(2)

        # 첫 페이지 이동
        self.target_url = f"{self.url}?pg=1" + query_str
        self.chrome_driver.move_page(self.target_url)
        time.sleep(2)

        # last page 계산
        total_cnt = self.chrome_driver.get_text('//*[@id="c-body"]/div/div[2]/div[1]/div[2]/p/strong')
        last_page = int(total_cnt) // 15 + 1

        # 첫페이지 파싱
        self.parse_data()

        # Loop : 페이지 돌면서 파싱
        for i in range(1, last_page):
            if self.is_last_data == True:
                break
            
            self.target_url = f"{self.url}?pg={i+1}" + query_str
            self.chrome_driver.move_page(self.target_url)
            time.sleep(2)
            
            self.parse_data()

        # excel
        self.write_excel()

        # close driver
        self.chrome_driver.close_driver()
        
        return {"result" : "완료 되었습니다."}
    
    def parse_data(self):
        """ 
        def description : 페이지 파싱

        Parameters
        ----------

        Returns
        -------
        result : 
        """

        for i in range(0, 15):

            # 예외처리
            if self.is_last_data == True:
                return
            
            # CASE 1 : 데이터가 더 이상 없을 경우
            if not self.chrome_driver.is_tag_exist(f'//*[@id="c-body"]/div/div[2]/div[1]/div[2]/table/tbody/tr[{i+1}]/td[1]'):
                self.is_last_data = True
                break
            
            # CASE 2 : 정상 데이터
            else:
                name = self.chrome_driver.get_text(f'//*[@id="c-body"]/div/div[2]/div[1]/div[2]/table/tbody/tr[{i+1}]/td[1]')
                name = name.split("\n")
                self.name_list.append(name[0])
                self.regnum_list.append(name[1])

                ceo_name = self.chrome_driver.get_text(f'//*[@id="c-body"]/div/div[2]/div[1]/div[2]/table/tbody/tr[{i+1}]/td[2]')
                self.ceo_name_list.append(ceo_name)

                addr = self.chrome_driver.get_text(f'//*[@id="c-body"]/div/div[2]/div[1]/div[2]/table/tbody/tr[{i+1}]/td[3]').replace("&nbsp;","")
                self.addr_list.append(addr)

                tel = self.chrome_driver.get_text(f'//*[@id="c-body"]/div/div[2]/div[1]/div[2]/table/tbody/tr[{i+1}]/td[4]')
                self.tel_list.append(tel)

                deduction_yn = self.chrome_driver.get_text(f'//*[@id="c-body"]/div/div[2]/div[1]/div[2]/table/tbody/tr[{i+1}]/td[5]')
                self.deduction_yn_list.append(deduction_yn)

        return

    def write_excel(self):
        """ 
        def description : 엑셀 저장

        Parameters
        ----------

        Returns
        -------
        result : 
        """

        # init
        file_name = "kar_" + str(generate_micro_date()) + ".xlsx"
        excel_path = f"{os.getcwd()}/../excel/"
        file_path = f"{excel_path}/{file_name}"
        if not os.path.isdir(excel_path):
            os.mkdir(excel_path)

        writer = pd.ExcelWriter(file_path, engine='openpyxl')
        
        """
        data frame 셋팅
        1. MAX_ROW 이상인 경우 sheet 분할
        2. standard_len이 각 배열 데이터의 길이와 일치하는 경우에만 셀로 사용
        3. standard_len은 name_list로 기준
        """

        # 길이 스탠다드 셋팅
        standard_len = 0
        if self.name_list:
            standard_len = len(self.name_list)
        
        if standard_len > 0:
        
            # flag set
            is_name = False
            if self.name_list:
                if standard_len == len(self.name_list):
                    is_name = True
            
            is_regnum = False
            if self.regnum_list:
                if standard_len == len(self.regnum_list):
                    is_regnum = True
            
            is_ceo_name = False
            if self.ceo_name_list:
                if standard_len == len(self.ceo_name_list):
                    is_ceo_name = True
            
            is_addr = False
            if self.addr_list:
                if standard_len == len(self.addr_list):
                    is_addr = True
            
            is_tel = False
            if self.tel_list:
                if standard_len == len(self.tel_list):
                    is_tel = True
            
            is_deduction_yn = False
            if self.deduction_yn_list:
                if standard_len == len(self.deduction_yn_list):
                    is_deduction_yn = True

            # LOOP
            for i in range(0, math.ceil(standard_len/MAX_ROW)):
                data_form = {}

                if is_name:
                    data_form['상호'] = self.name_list[i*MAX_ROW:(i+1)*MAX_ROW]

                if is_regnum:
                    data_form['개설등록번호'] = self.regnum_list[i*MAX_ROW:(i+1)*MAX_ROW]
                
                if is_ceo_name:
                    data_form['대표자'] = self.ceo_name_list[i*MAX_ROW:(i+1)*MAX_ROW]
                
                if is_addr:
                    data_form['사무실 소재지'] = self.addr_list[i*MAX_ROW:(i+1)*MAX_ROW]
                
                if is_tel:
                    data_form['전화번호'] = self.tel_list[i*MAX_ROW:(i+1)*MAX_ROW]
                
                if is_deduction_yn:
                    data_form['공제가입여부'] = self.deduction_yn_list[i*MAX_ROW:(i+1)*MAX_ROW]

                if data_form:
                    data_frame = pd.DataFrame(data_form)
                    sheet_name = 'sheet' + str(i+1)
                    data_frame.to_excel(writer, sheet_name=sheet_name)
            
            # write excel
            writer.save()
        return