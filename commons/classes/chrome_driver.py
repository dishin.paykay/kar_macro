from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

import chromedriver_autoinstaller

import pyautogui
import pyperclip

from commons.exception.exception import *
from commons.function.common_function import *

import subprocess
import shutil
import os

chrome_ver = chromedriver_autoinstaller.get_chrome_version().split('.')[0]  #크롬드라이버 버전 확인

class ChromeDriver:

    def __init__(self):
        self.driver = None      # 크롬 드라이버
        self.chrome_ver = None  # 크롬 드라이버 버전

    def install_new_ver_driver(self):
        """
        def description : 크롬 드라이버 설치
        """

        cwd = os.getcwd() 
        driver_path = cwd + f"/driver/"
        chromedriver_autoinstaller.install(False, path = driver_path)
        return

    def set_driver(self):
        """ 
        def description : 크롬 드라이버 셋팅

        Parameters
        ----------

        Returns
        -------
        result : driver(obj)
        """

        # 크롬 드라이버 옵션 셋팅
        WINDOWSIZE = "1920,1080"
        chrome_options = Options()
        #chrome_options.add_argument( "--headless" )                                    # 크롬창이 열리지 않음
        chrome_options.add_argument( "--no-sandbox" )                                   # GUI를 사용할 수 없는 환경에서 설정, linux, docker 등
        chrome_options.add_argument( "--disable-gpu" )                                  # GUI를 사용할 수 없는 환경에서 설정, linux, docker 등
        chrome_options.add_argument(f"--window-size={ WINDOWSIZE }")                    # 윈도 사이즈
        chrome_options.add_argument('Content-Type=application/json; charset=utf-8')     # 언어 셋
        chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])   # 웹드라이버 프로세스 다운 방지

        try:
            driver_path = f"{os.getcwd()}/driver/{chrome_ver}/chromedriver.exe"
            self.driver = webdriver.Chrome(driver_path, options=chrome_options)
            
        except:
            chromedriver_autoinstaller.install(True)
            self.driver = webdriver.Chrome(f'{driver_path}/{chrome_ver}/chromedriver.exe', options=chrome_options)

        return self.driver

    def set_avoided_drive(self):
        """ 
        def description : 크롬 드라이버 우회하여 셋팅

        Parameters
        ----------

        Returns
        -------
        result : driver(obj)
        """

        try:
            shutil.rmtree(r"C:\chrometemp")  # remove Cookie, Cache files

        except FileNotFoundError:
            pass

        try:
            subprocess.Popen(r'C:\Program Files\Google\Chrome\Application\chrome.exe --remote-debugging-port=9222 '
                            r'--user-data-dir="C:\chrometemp"')   # Open the debugger chrome
            
        except FileNotFoundError:
            subprocess.Popen(r'C:\Users\binsu\AppData\Local\Google\Chrome\Application\chrome.exe --remote-debugging-port=9222 '
                            r'--user-data-dir="C:\chrometemp"')

        option = Options()
        option.add_experimental_option("debuggerAddress", "127.0.0.1:9222")

        cwd = os.getcwd() 
        driver_path = cwd + f"/driver/"

        try:
            self.driver = webdriver.Chrome(f'{driver_path}/{chrome_ver}/chromedriver.exe', options=option)
            
        except:
            chromedriver_autoinstaller.install(True)
            self.driver = webdriver.Chrome(f'{driver_path}/{chrome_ver}/chromedriver.exe', options=option)

        return self.driver

    def get_latest_chrome_ver(self):
        
        """ 
        def description : 크롬 드라이버 최신버전 확인

        Parameters
        ----------

        Returns
        -------
        chrome_ver : 크롬 최신 버전(str)
        """

        self.chrome_ver = chromedriver_autoinstaller.get_chrome_version().split('.')[0]  #크롬드라이버 버전 확인
        return self.chrome_ver
    
    def get_driver(self):
        """ 
        def description : 크롬 드라이버 조회 

        Parameters
        ----------

        Returns
        -------
        chrome_ver : driver(obj)
        """

        return self.driver

    def move_page(self, url):
        """ 
        def description : 페이지 이동

        Parameters
        ----------
        url : 이동할 페이지 url

        Returns
        -------
        """

        if not self.driver:
            raise NotFoundException(msg=f"크롬 드라이버가 존재하지 않습니다")

        # 검색어에 해당하는 url 접근
        self.driver.get(url) 
        return
    
    def set_select_box(self, tag, value):
        """ 
        def description : 셀렉박스 셋팅

        Parameters
        ----------
        tag : 셀렉박스 태그
        value : 셀렉박스 선택값

        Returns
        -------
        """

        dropdown = Select(self.driver.find_element_by_xpath(tag))  #정렬기준 드롭다운
        dropdown.select_by_value(value)

        return
    
    def set_input(self, tag, value):
        """ 
        def description : 인풋박스 셋팅

        Parameters
        ----------
        tag : 인풋박스 태그
        value : 인풋박스 값

        Returns
        -------
        """

        self.driver.find_element_by_xpath(tag).click()
        time.sleep(1)
        
        pyperclip.copy(value)
        pyautogui.hotkey("ctrl", "v") # 한글이 깨지므로 클립보드로 우회.

        return
    
    def get_text(self, tag):
        """ 
        def description : 태그 문구 조회

        Parameters
        ----------
        tag : 태그

        Returns
        -------
        text : 문구 (str)
        """
        
        text  = self.driver.find_element(By.XPATH, tag).text
        return text

    def click_button(self, tag):
        """ 
        def description : 버튼 클릭

        Parameters
        ----------
        tag : 태그

        Returns
        -------
        """

        self.driver.find_element_by_xpath(tag).click()
        return
    
    def is_tag_exist(self, tag):
        """ 
        def description : 태그 존재 여부 확인

        Parameters
        ----------
        tag : 태그

        Returns
        -------
        결과 : boolean
        """
        
        result = False 

        try : 
            self.driver.find_element(By.XPATH, tag)
            result = True

        except Exception as ex :
            
            if "Message: no such element: Unable to locate element" in str(ex)[0:100]:
                pass

            elif "Message: element click intercepted: Element" in str(ex)[0:100]:
                pass

        return result

    def close_driver(self):
        """ 
        def description : 롬 드라이버 종료 

        Parameters
        ----------
        tag : 태그

        Returns
        -------
        """

        if not self.driver:
            raise NotFoundException(msg=f"크롬 드라이버가 존재하지 않습니다")

        try:
            self.driver.close()
            self.driver.quit() 
        
        except Exception as ex:
            print(ex)

        return