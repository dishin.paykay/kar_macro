from commons.exception.exception import *
from service.main_service import *

class Route():

    def __init__(self):

        self.url = None
        self.service_div = None

    # ==========================================================
    # route
    # ==========================================================
    def route(self, input):
        """ 
        def description : 라우트
        -------
        """
        self.service_div = input["service_div"]

        if self.service_div == "00":
            ms = MainService()
            return ms.main(input)

        # not found
        raise NotFoundException(msg=f"잘못 된 요청입니다")
